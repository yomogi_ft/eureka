﻿using UnityEngine;
using System.Collections;

public class TrailManager : MonoBehaviour {
	
	public Vector3 spawnArea;
	
	public TrailBehaviour prefab;
	
	public float spawnSpan;
	public float spawnAmount;
	
	
	// Use this for initialization
	void Start ()
	{
		StartCoroutine(SpawnRoutine());
	}
	
	public IEnumerator SpawnRoutine()
	{
		while(this.enabled) {
			for(int i=0; i<this.spawnAmount; i++){			
				var pos = new Vector3(
					Random.Range(-spawnArea.x, spawnArea.x),
					Random.Range(-spawnArea.y, spawnArea.y),
					Random.Range(-spawnArea.z, spawnArea.z)
				);				
				var obj = Instantiate(prefab, pos + this.transform.position, Quaternion.Euler(0, Random.Range(-90, 90),0)) as TrailBehaviour;
				obj.transform.parent = this.transform;
			}
			
			yield return new WaitForSeconds(this.spawnSpan);
		}
	}
		
}
