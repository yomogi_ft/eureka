﻿using UnityEngine;
using System.Collections;

public class ImageEffectLauncher : MonoBehaviour 
{
	void Start ()
	{
		var component = Camera.main.gameObject.AddComponent<TransactionEffector>();
		var original = this.GetComponent<TransactionEffector>();
		component.animationLength = original.animationLength;
		component.thresholdChange = original.thresholdChange;
		component.animationAccelarate = original.animationAccelarate;
		component.material = original.material;
	}
	
	
}
