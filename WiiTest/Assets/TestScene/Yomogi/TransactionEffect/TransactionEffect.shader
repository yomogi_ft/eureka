﻿Shader "Custom/TransactionEffect" {
    Properties {
		_MainTex ("Screen Blended", 2D) = "" {}    
        _Color1 ("Top Color", Color) = (1, 1, 1, 0)
        _Color2 ("Horizon Color", Color) = (1, 1, 1, 0)
        _Threshold("Threshold", Range(0, 1)) = 0
    }
    SubShader {
	Pass {    
    
		CGPROGRAM 
	    #pragma vertex vert
	    #pragma fragment fragAlphaBlend
		#include "UnityCG.cginc"
			
		struct v2f {
			float4 pos : POSITION;
			float2 uv[2] : TEXCOORD0;
		};

        float4 _Color1;
        float4 _Color2;
        float _Threshold;
        
		sampler2D _MainTex;
		
		half4 _MainTex_TexelSize;
			
		v2f vert( appdata_img v ) { 
			v2f o;
			o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
			o.uv[0] =  v.texcoord.xy;
			
			#if UNITY_UV_STARTS_AT_TOP
			if(_MainTex_TexelSize.y<0.0)
				o.uv[0].y = 1.0-o.uv[0].y;
			#endif
			
			o.uv[1] =  v.texcoord.xy;	
			return o;
		}
		
		half4 fragAlphaBlend (v2f i) : COLOR
		{
			float r = sqrt(pow(i.uv[0].x * 2 - 1, 2) + pow(i.uv[0].y * 2 - 1 , 2)) / sqrt(2.0);
			
			float a = -2 * _Threshold + 2;

			half4 toAdd = _Color1 * r * a + _Color2 * (1-r) * a;
			
			return lerp(tex2D(_MainTex, i.uv[1]), toAdd, min((2-a)/(2-1.5), 1));
		}	
        ENDCG
        }
   } 
}
