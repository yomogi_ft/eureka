﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TransactionEffector : MonoBehaviour 
{	
	public Material material;
		
	public AnimationCurve thresholdChange;
	public float animationLength;
	public float animationAccelarate; 
	
	private float threshold;
	private float currentTime;
	
	
	// Use this for initialization
	void Start () 
	{
		currentTime = 0;
	}
	
	void Update()
	{
		if (currentTime <= animationLength) {
			threshold = thresholdChange.Evaluate(currentTime);
 			currentTime += Time.deltaTime * animationAccelarate;
		} else {
			Destroy(this);
		}
	}
	
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
		material.SetFloat ("_Threshold", threshold);
		
		Graphics.Blit (source, destination, material);
	}
}
