﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaterTileUpdater : MonoBehaviour
{
	enum Neighbor
	{
		Forward,
		Back,
		Right,
		Left
	}
	
	Dictionary<Neighbor, WaterTileUpdater> neighbors;	
	
	// Use this for initialization
	void Start ()
	{
		this.neighbors = new Dictionary<Neighbor, WaterTileUpdater>();
		
		this.neighbors.Add(Neighbor.Forward, FindNeighbor(Vector3.forward));
		this.neighbors.Add(Neighbor.Back,    FindNeighbor(Vector3.back));
		this.neighbors.Add(Neighbor.Right,   FindNeighbor(Vector3.right));
		this.neighbors.Add(Neighbor.Left,    FindNeighbor(Vector3.left));
	}
	
	private WaterTileUpdater FindNeighbor(Vector3 direction)
	{
		Collider[] col = Physics.OverlapSphere(this.transform.position + direction * collider.bounds.size.x, 0.1f, 1 << gameObject.layer);
		if (col.Length != 1) {
			return null;
		}
		
		var res = col[0].GetComponent<WaterTileUpdater>();
		return res;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void OnTriggerExit(Collider col)
	{
		var localPos = col.transform.position - this.transform.position;
		
		if (Mathf.Abs(localPos.z) > Mathf.Abs(localPos.x)) { // forward or back
			if (Mathf.Sign(localPos.z) < 0 ) {				
				this.TryRemoveTile(Neighbor.Forward);
				this.neighbors[Neighbor.Left].TryRemoveTile(Neighbor.Forward);
				this.neighbors[Neighbor.Right].TryRemoveTile(Neighbor.Forward);				
				
				this.neighbors[Neighbor.Back].TryAddTile(Neighbor.Back, Vector3.back);
				this.neighbors[Neighbor.Back].neighbors[Neighbor.Left].TryAddTile(Neighbor.Back, Vector3.back);
				this.neighbors[Neighbor.Back].neighbors[Neighbor.Right].TryAddTile(Neighbor.Back, Vector3.back);			
			} else {
				this.TryRemoveTile(Neighbor.Back);
				this.neighbors[Neighbor.Left].TryRemoveTile(Neighbor.Back);
				this.neighbors[Neighbor.Right].TryRemoveTile(Neighbor.Back);
				
				this.neighbors[Neighbor.Forward].TryAddTile(Neighbor.Forward, Vector3.forward);
				this.neighbors[Neighbor.Forward].neighbors[Neighbor.Left].TryAddTile(Neighbor.Forward, Vector3.forward);
				this.neighbors[Neighbor.Forward].neighbors[Neighbor.Right].TryAddTile(Neighbor.Forward, Vector3.forward);			
			}
		} else { // right or left
			if (Mathf.Sign(localPos.x) < 0 ) {
				this.TryRemoveTile(Neighbor.Right);
				this.neighbors[Neighbor.Forward].TryRemoveTile(Neighbor.Right);
				this.neighbors[Neighbor.Back].TryRemoveTile(Neighbor.Right);
 				
				this.neighbors[Neighbor.Left].TryAddTile(Neighbor.Left, Vector3.left);
				this.neighbors[Neighbor.Left].neighbors[Neighbor.Forward].TryAddTile(Neighbor.Left, Vector3.left);
				this.neighbors[Neighbor.Left].neighbors[Neighbor.Back].TryAddTile(Neighbor.Left, Vector3.left);
			} else {
				this.TryRemoveTile(Neighbor.Left);
				this.neighbors[Neighbor.Forward].TryRemoveTile(Neighbor.Left);
				this.neighbors[Neighbor.Back].TryRemoveTile(Neighbor.Left);
				
				this.neighbors[Neighbor.Right].TryAddTile(Neighbor.Right, Vector3.right);
				this.neighbors[Neighbor.Right].neighbors[Neighbor.Forward].TryAddTile(Neighbor.Right, Vector3.right);
				this.neighbors[Neighbor.Right].neighbors[Neighbor.Back].TryAddTile(Neighbor.Right, Vector3.right);
			}
		}
	}
	
	private void TryRemoveTile(Neighbor key)
	{
		if (this.neighbors[key] == null) {
			return;
		}		
				
		var del = this.neighbors[key];
		this.neighbors[key] = null;
		Destroy(del.gameObject);
	}
	
	private void TryAddTile(Neighbor key, Vector3 direction)
	{
		if (this.neighbors[key] != null) {
			return; 
		}
				
		var tile = Instantiate(this.gameObject, this.transform.position + direction * this.collider.bounds.size.x, Quaternion.identity) as GameObject;
		tile.name = "Water";
		var compo = tile.GetComponent<WaterTileUpdater>();
		if (compo == null) {
			return;
		}
		this.neighbors[key] = compo;
	}
	
}
 