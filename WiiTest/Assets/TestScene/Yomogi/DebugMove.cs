﻿using UnityEngine;
using System.Collections;

public class DebugMove : MonoBehaviour {
	
	public float speed;
		
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position += speed * (Input.GetAxis("Vertical") * Vector3.forward + Input.GetAxis("Horizontal") * Vector3.right);
	}
}
