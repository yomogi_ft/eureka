﻿using UnityEngine;
using System.Collections;

public class TrailBehaviour : MonoBehaviour
{
	TrailRenderer trail;
	
	public float speed;
	
 	public float angleSpeed = 15;
	
	public float lifeTime = 5;

	// Use this for initialization
	void Start () 
	{
		this.trail = GetComponent<TrailRenderer>();
 	}
	
	// Update is called once per frame
	void Update ()
	{
		this.transform.position += this.transform.forward * speed;
		
		this.transform.Rotate(Vector3.up, Random.Range(-angleSpeed, angleSpeed));
		
		this.lifeTime -= Time.deltaTime;
		if (this.lifeTime < 0) {
			Destroy(gameObject);
		}
	}
}
