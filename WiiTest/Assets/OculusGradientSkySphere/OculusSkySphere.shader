﻿Shader "Custom/OculusSkySphere" {
	Properties {
        _Color1 ("Top Color", Color) = (1, 1, 1, 0)
        _Color2 ("HorizonUpper Color", Color) = (1, 1, 1, 0)
        _Color3 ("HorizonDown Color", Color) = (1, 1, 1, 0)
        _Color4 ("Bottom Color", Color) = (1, 1, 1, 0)
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader { 
		Tags { "Queue"="Background"}
		ZWrite Off
		Fog { Mode Off }      
		Lighting Off
		
		CGPROGRAM
		#pragma surface surf SimpleLambert
		struct Input {
		  float2 uv_MainTex;
		};
		
		half4 LightingSimpleLambert (SurfaceOutput s, half3 lightDir, half atten) {
			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
	    }
		
        float4 _Color1; 
        float4 _Color2;
        float4 _Color3;
        float4 _Color4;
 		void surf (Input IN, inout SurfaceOutput o)
 		{
	        float p  = IN.uv_MainTex.y * 2 - 1;
	        float p1 = max(0.0f, 2 * p - 1);
	        float p2 = 1 - abs(p - 0.5f) + min(0.0f, 0.5f - p);   
 	        float p3 = 1 - abs(p + 0.5f) + min(0.0f, 0.5f + p);
	        float p4 = max(0.0f, -2 * p - 1);
             
			o.Albedo = _Color1 * p1 + _Color2 * p2 + _Color3 * p3 + _Color4 * p4;
		}
		ENDCG			
	}
	Fallback "Diffuse" 
 }