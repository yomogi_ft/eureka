﻿using UnityEngine;
using System.Collections;

public class CameraTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = transform.parent.position;
		if(Input.GetKey(KeyCode.A)){
			transform.eulerAngles += new Vector3(0.0f,-1.0f,0.0f);
		}else if(Input.GetKey(KeyCode.D)){
			transform.eulerAngles += new Vector3(0.0f,1.0f,0.0f);
		}else if(Input.GetKey(KeyCode.W)){
			transform.eulerAngles += new Vector3(-1.0f,0.0f,0.0f);
		}else if(Input.GetKey(KeyCode.S)){
			transform.eulerAngles += new Vector3(1.0f,0.0f,0.0f);
		}
	}
}
