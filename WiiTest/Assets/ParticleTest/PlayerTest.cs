﻿using UnityEngine;
using System.Collections;

public class PlayerTest : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		transform.Translate(transform.forward/10.0f);
		if(Input.GetKey(KeyCode.LeftArrow)){
			transform.Rotate(new Vector3(0.0f,-1.0f,0.0f));
		}else if(Input.GetKey(KeyCode.RightArrow)){
			transform.Rotate(new Vector3(0.0f,1.0f,0.0f));
		}else if(Input.GetKey(KeyCode.UpArrow)){
			transform.Rotate(new Vector3(-1.0f,0.0f,0.0f));
		}else if(Input.GetKey(KeyCode.DownArrow)){
			transform.Rotate(new Vector3(1.0f,0.0f,0.0f));
		}
	}
	
}
