﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {
	public GameObject splash;
	
	public GameObject waterEffect_30;
	public GameObject waterEffect_45;
	
	public GameObject waterOut_30;
	public GameObject waterOut_45;
	
	private float speed;
	private GameObject tmp;
	
	void Start(){
		speed = 10.0f;
	}
	// Use this for initialization
	void OnTriggerStay(Collider col){
		tmp = Instantiate(splash) as GameObject;
		tmp.transform.position = col.transform.position + transform.forward;
		Destroy(tmp,3.0f);
	}
	
	void  OnTriggerEnter(Collider col){
		Vector3 vec = col.transform.forward;
		Vector3 forward = transform.forward;
		float rad = (vec.x*forward.x + vec.y*forward.y + vec.z*forward.z);
		if(col.transform.forward.y < 0.0f){
			if(rad <= Mathf.Cos(Mathf.PI/4.0f) && rad >= Mathf.Cos(Mathf.PI*3.0f/4.0f) ){
				tmp = Instantiate(waterEffect_45) as GameObject;
				tmp.transform.position = col.transform.position + col.transform.forward * speed;
				tmp.transform.parent = null;
				Destroy(tmp,10.0f);
			}else if(rad <= Mathf.Cos(Mathf.PI/6.0f) && rad >= Mathf.Cos(Mathf.PI*5.0f/6.0f)){
				tmp = Instantiate(waterEffect_30) as GameObject;
				tmp.transform.position = col.transform.position + col.transform.forward * speed;
				tmp.transform.parent = null;
				Destroy(tmp,10.0f);
			}
		}else{
			if(rad <= Mathf.Cos(Mathf.PI/4.0f) && rad >= Mathf.Cos(Mathf.PI*3.0f/4.0f) ){
				tmp = Instantiate(waterOut_45) as GameObject;
				tmp.transform.position = col.transform.position + col.transform.forward;
				tmp.transform.parent = null;
				Destroy(tmp,10.0f);
			}else if(rad <= Mathf.Cos(Mathf.PI/6.0f) && rad >= Mathf.Cos(Mathf.PI*5.0f/6.0f)){
				tmp = Instantiate(waterOut_30) as GameObject;
				tmp.transform.position = col.transform.position + col.transform.forward;
				tmp.transform.parent = null;
				Destroy(tmp,10.0f);
			}
		}
	}
}
